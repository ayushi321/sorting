package Sorting;

import java.util.Scanner;

public class SortingofNumbers {
    private int arr[];
    private int n;
    private int t;

    public SortingofNumbers() {  //Default constructor
        this.arr=null;
        this.n=0;
        this.t=0;
    }

    public SortingofNumbers(int[] arr, int n, int t) { // Parametrized constructor
        this.arr = arr;
        this.n = n;
        this.t = t;
    }
    // methods

    public void getvalues(){
        Scanner sc= new Scanner(System.in);
        System.out.println("Enter number of elements: ");
        n=sc.nextInt();
        arr=new int[n];
        System.out.println("Enter elements in array: ");
        for(int i=0;i<n;i++)
            arr[i]=sc.nextInt();
    }
    public void result(){


    }
// Getters and Setters

    public int[] getArr() {
        return arr;
    }

    public void setArr(int[] arr) {
        this.arr = arr;
    }

    public int getN() {
        return n;
    }

    public void setN(int n) {
        this.n = n;
    }

    public int getT() {
        return t;
    }

    public void setT(int t) {
        this.t = t;
    }
}
